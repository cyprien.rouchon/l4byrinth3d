import socket
import ngrok
import requests

token = "2dgHMxmhrmusAFYCl0CufJuCnK5_2wVPYGYgHebt1jiEwA3TA"

def creer_un_tunnel(port):
    """
    creer un tunnel pour que des clients se connectent à travers different serveurs
    :param port: le port que l'on veut ouvrir (int)
    :return: objet ngrok qui correspond au tunnel
    """
    listener = ngrok.forward(f"localhost:{port}", authtoken=token, proto="tcp")
    print(f"Ingress established at: {listener.url()}")
    return listener.url().split("//")[1]

def fermer_un_tunnel(tunnel):
    """
    fermer un tunnel existant
    :param tunnel: objet ngrok
    :return: la fermeture du tunnel
    """
    return tunnel.close()

def get_public_ip():
    """
    permet d'obtenir l'ip publique
    :return: ip publique ou une erreur (str)
    """
    try:
        ip = requests.get('https://api.ipify.org?format=json')
        ip = ip.text
        ip = ip.split(":")[1]
        ip = ip[1:-2]
        return ip
    except Exception as error:
        return error


class Serveur:
    def __init__(self,port):
        """
        __init__ permet d'initialiser un objet server
        :param hote: un str qui correspond à l'adresse du serveur
        :param port: un int qui correspond au port du serveur
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(("0.0.0.0", port))
        self.socket.listen(1)
        self.client, self.address = self.socket.accept()
        print (f"{self.address} connected")

    def send(self,msg):
        """
        send envoie un message sur la connexion
        :param msg: str
        :return: l'envoie du message
        """
        return self.client.send(msg.encode("utf-8"))
    
    def recv(self,size=255):
        """
        recv reçoit le message depuis la connexion
        :param size: taille maximal du message reçu
        :return: str, le message reçu
        """ 
        return self.client.recv(size).decode("utf-8")
    
    def send_lab(self,lab):
        """
        send_lab envoie le labyrinthe sur la connexion
        :param lab: le labyrinthe
        :return: l'envoie du labyrinthe
        """
        msg = ""
        for ligne in lab:
            for case in ligne:
                msg += str(f"{case[0]},{case[1]},{case[2]},{case[3]};")
            msg = msg[:-1]
            msg += "_"
        msg = msg[:-1]
        return self.client.send(msg.encode("utf-8"))
    
    def send_case_sortie(self,case_sortie):
        """
        send_case_sortie envoie la case de sortie du labyrinthe sur la connexion
        :param case_sortie: la case de sortie
        :return: l'envoie de la case de sortie
        """
        x,y = case_sortie
        msg = f"{x},{y}"
        return self.client.send(msg.encode("utf-8"))

    
    def send_pseudo(self,pseudo):
        """
        envoie de la liste de correspondance entre id et pseudo
        :param pseudo: dictionnaire
        :return: l'envoie des pseudo
        """
        pseudo = str(pseudo)[1:-1]
        return self.client.send(pseudo.encode("utf-8"))


class Client:
    def __init__(self,hote,port,to=2):
        """
        __init__ permet d'initialiser un objet client
        :param hote: un str qui correspond à l'adresse du serveur
        :param port: un int qui correspond au port du serveur
        :param to: un int qui correspond au time out (le temps avant qu'une connexion échue)
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(to) # time out defini seulement pour la 1ère connexion
        self.socket.connect((hote, port))
        self.socket.settimeout(None) # le time out redevient infini
        print(f"Connection on {port}")

    def send(self,msg):
        """
        send envoie un message sur la connexion
        :param msg: str
        :return: l'envoie du message
        """
        return self.socket.send(msg.encode("utf-8"))
    
    def recv(self,size=255):
        """
        recv reçoit le message depuis la connexion
        :param size: taille maximal du message reçu
        :return: str, le message reçu
        """ 
        return  self.socket.recv(size).decode("utf-8")
    
    def recv_lab(self,size=2**20): #size très grand pour pouvoir recevoir le labyrinthe en entier
        """
        recv_lab reçoit le labyrinthe depuis la connexion
        :param size: taille maximal du message reçu
        :return: le labyrinthe reçu (liste de liste de liste de liste de bool)
        """ 
        lab = self.socket.recv(size).decode("utf-8")
        lab = lab.split("_")
        for i,ligne in enumerate(lab):
            lab[i] = ligne.split(";")
            for t,case in enumerate(lab[i]):
                lab[i][t] = case.split(",")
                for e,val in enumerate(lab[i][t]):
                    if val == "True": lab[i][t][e] = True
                    else: lab[i][t][e] = False
        return lab
    
    def recv_case_sortie(self,size=255):
        """
        recv_case_sortie reçoit la case sortie depuis la connexion
        :param size: taille maximal du message reçu
        :return: la case sortie reçu
        """ 
        case_sortie = self.socket.recv(size).decode("utf-8")
        case_sortie = case_sortie.split(",")
        return case_sortie

    def recv_pseudo(self,size=225):
        """
        rçoit le dictionnaire de correspondance entre id et pseudo
        :return: le dictionnaire de correspondance entre id et pseudo
        """
        dict_pseudo = {}
        pseudo = self.socket.recv(size).decode("utf-8")
        pseudo = pseudo.split(',')

        for i,e in enumerate(pseudo):
            e = e.split(":")
            if i == 0:  dict_pseudo[e[0]] = e[1][2:-1]
            else: dict_pseudo[e[0][1]] = e[1][2:-1]

        return dict_pseudo  
    

if __name__ == "__main__":

    tunnel = creer_un_tunnel(4444)
    serveur = Serveur(4444,'0.0.0.0')
    print(serveur.recv())