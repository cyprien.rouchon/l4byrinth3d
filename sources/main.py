from customtkinter import *
from start_game import startGame
from lab3D_serveur import mainServ
from lab3D_client import initServer, waitingServer, recv_lab_pseudo
from divers import creer_un_thread
import bot
import network
import atexit

current_widgets = {}
labSize, nbPlayer, clientID, client, pseudo, serverName = [None] * 6
lstPseudo = []


app = CTk()
app.geometry("600x400")
app.after(0, lambda:app.state('zoomed'))
app.wm_attributes('-fullscreen', True)
HEIGHT, WIDTH = app.winfo_screenheight(), app.winfo_screenwidth()
set_appearance_mode("dark")

def slider_handler_size(value:float): 
    """modifie le texte en dessous du slider pour la taille du labyrinthe"""
    global current_widgets
    
    label = current_widgets["labelSlideSize"]
    label.configure(text=f"{int(value)}x{int(value)}")

def slider_handler_player(value:float):
    """modifie le texte en dessous du slider pour le nombre de joueurs"""
    label = current_widgets["labelSlidePlayer"]
    label.configure(text=f"{int(value)} joueurs")

def createGame():
    """Créer le serveur et le client de celui qui vient de créer la partie et affiche le menu d'attente"""
    global current_widgets, labSize, nbPlayer, clientID, pseudo, serverName

    pseudo = current_widgets["entryPseudo"].get()
    if pseudo == "":
        labelPseudo = current_widgets["labelPseudo"]
        labelPseudo.configure(text="Vous devez d'abord entrer un pseudo", text_color="red", font=("Arial", 20))
        return

    serverName = current_widgets["entryServerName"].get()
    if serverName == "":
        labelServer = current_widgets["labelServer"]
        labelServer.configure(text="Vous devez d'abord entrer le nom du server", text_color="red", font=("Arial", 20))
        serverName = None
        return
    
    nbPlayer = int(current_widgets["sliderPlayer"].get())
    addr_list = []
    for i in range(nbPlayer):
        addr = network.creer_un_tunnel(4440+i)
        addr_list.append(addr)

    if not bot.check_name(serverName):
        labelServer = current_widgets["labelServer"]
        labelServer.configure(text="un serveur identique existe deja", text_color="red", font=("Arial", 20))
        return

    labSize = current_widgets["sliderSize"].get()
    checkboxFS = current_widgets["checkboxFS"]
    FS = not bool(checkboxFS.get())
    checkboxPVP = current_widgets["checkboxPVP"]
    PVP = bool(checkboxPVP.get())
    bot.add_server(f"{serverName}|init addr")

    atexit.register(bot.supr_server,server_name = serverName)

    clientID = 0
    changeWindow("waiting")
    creer_un_thread(lambda: mainServ(nbPlayer, labSize, serverName,addr_list, PVP))
    creer_un_thread(lambda: joinGame(FS, True))

def joinGame(FS = None, clientIsServer:bool = False):
    """Essaie de créer une connexion entre le client et le serveur
       Si c'est un succès, on affiche le menu d'attente
       Sinon on affiche qu'il y'a eu une erreur
    """
    global current_widgets, serverName, labSize, nbPlayer, clientID, client, pseudo, lstPseudo

    if pseudo == None:
        pseudo = current_widgets["entryPseudo"].get()[0:14]
        if pseudo == "":
            labelPseudo = current_widgets["labelPseudo"]
            labelPseudo.configure(text="Vous devez d'abord entrer un pseudo", text_color="red", font=("Arial", 20))
            return

    if serverName == None:
        serverName = current_widgets["entryServerName"].get()[0:14]
        if serverName == "":
            labelServer = current_widgets["labelServer"]
            labelServer.configure(text="Vous devez d'abord entrer le nom du server", text_color="red", font=("Arial", 20))
            return

    if not clientIsServer: # Vérifie si le client n'est pas celui qui créer le serveur
        button = current_widgets["btnJoin"]
        button.configure(state="disabled")
        #! -------------------------------------------------
        checkboxFS = current_widgets["checkboxFS"]
        FS = not bool(checkboxFS.get())
        
    succes = initServer(pseudo,serverName)

    if succes[0]:
        client = succes[1]
        clientID = succes[2]
        nbPlayer = succes[3]
        lstPseudo = succes[4]
        PVP = succes[5]
        print(succes)

        if clientID != 0: changeWindow("waiting")
        if clientID != nbPlayer-1: waitingServer(client, nbPlayer, current_widgets)
        print("LANCEMENT DE LA PARTIE")
        recv_lab_pseudo(client,nbPlayer,clientID, FS, PVP)

    else:
        label = current_widgets["labelEntry"]
        label.configure(text="Erreur: nom de serveur incorrect ou serveur inacceccible", text_color="red")
        button.configure(state="normal")

def playSolo():
    """Lance la partie en mode solo"""
    global app, current_widgets

    checkboxFS = current_widgets["checkboxFS"]
    FS = not bool(checkboxFS.get())
    mazeSide = current_widgets["sliderSize"].get()
    startGame(FS, solo=True, mazeSide=int(mazeSide))

def menu_solo(): 
    """Crée le menu pour jouer en solo"""
    global current_widgets

    btnBack = CTkButton(master=app, text="Retour", font=("Arial", 30), command=lambda: changeWindow("main"))
    btnBack.place(relx=0.1, rely=0.1)

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.2, anchor="center")

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=350)
    frame.pack(expand=True)

    label2 = CTkLabel(master=frame, text="Choisissez la taille du labyrinthe:", font=("Arial", 40), text_color="#B9C0CC")
    label2.place(relx=0.5, rely=0.2, anchor="center")

    sliderSize = CTkSlider(master=frame, from_=3, to=50, number_of_steps=47, button_color="#C850C0", progress_color="#C850C0", orientation="horizontal", command=slider_handler_size)
    sliderSize.place(relx=0.5, rely=0.4, anchor="center")
    sliderSize.set(8)

    labelSlideSize = CTkLabel(master=frame, text="8x8", font=("Arial", 20), text_color="#B9C0CC")
    labelSlideSize.place(relx=0.5, rely=0.55, anchor="center")

    btnPlay = CTkButton(master=frame, text="Jouer", font=("Arial", 30), command=playSolo)
    btnPlay.place(relx=0.5, rely= 0.7, anchor="center") 

    checkboxFS = CTkCheckBox(master=frame, text="Afficher la modélisation 2D", font=("Arial", 25), text_color="#B9C0CC")
    checkboxFS.place(relx=0.5, rely=0.85, anchor="center")

    current_widgets = {"label": label, "frame": frame, "label2": label2, "sliderSize": sliderSize, "labelSlideSize": labelSlideSize, "btnPlay": btnPlay, "btnBack": btnBack, "checkboxFS": checkboxFS}

def menu_multi():
    """Crée le menu pour jouer en multi"""
    global current_widgets

    btnBack = CTkButton(master=app, text="Retour", font=("Arial", 30), command=lambda: changeWindow("main"))
    btnBack.place(relx=0.1, rely=0.1)

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.2, anchor="center")

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=250)
    frame.pack(expand=True)

    btnHost = CTkButton(master=frame, text="Créer une partie", font=("Arial", 30), corner_radius=5, fg_color="#3F5477", hover_color="#4158D0", border_color="#FFCC70", border_width=2,command=lambda: changeWindow("host"), border_spacing=10)
    btnHost.place(relx=0.3, rely=0.7, anchor="center")

    btnGuest = CTkButton(master=frame, text="Rejoindre une parie", font=("Arial", 30), corner_radius=5, fg_color="#3F5477", hover_color="#4158D0", border_color="#FFCC70", border_width=2, command=lambda: changeWindow("guest"), border_spacing=10)
    btnGuest.place(relx=0.7, rely=0.7, anchor="center")
    current_widgets = {"label": label, "frame": frame, "btnHost": btnHost, "btnGuest": btnGuest, "btnBack": btnBack}

def menu_host():
    """Crée le menu pour créer une partie multijoueur"""
    global current_widgets, WIDTH, HEIGHT

    btnBack = CTkButton(master=app, text="Retour", font=("Arial", 30), command=lambda: changeWindow("multi"))
    btnBack.place(relx=0.1, rely=0.1)

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.2, anchor="center")

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=0.7*HEIGHT)
    frame.pack(expand=True)

    label2 = CTkLabel(master=frame, text="Choisissez la taille du labyrinthe", font=("Arial", 40), text_color="#B9C0CC")
    label2.place(relx=0.5, rely=0.1, anchor="center")

    sliderSize = CTkSlider(master=frame, from_=3, to=50, number_of_steps=47, button_color="#C850C0", progress_color="#C850C0", orientation="horizontal", command=slider_handler_size)
    sliderSize.place(relx=0.5, rely=0.22, anchor="center")
    sliderSize.set(8)

    labelSlideSize = CTkLabel(master=frame, text="8x8", font=("Arial", 20), text_color="#B9C0CC")
    labelSlideSize.place(relx=0.5, rely=0.28, anchor="center")

    sliderPlayer = CTkSlider(master=frame, from_=2, to=4, number_of_steps=2, button_color="#C850C0", progress_color="#C850C0", orientation="horizontal", command=slider_handler_player)
    sliderPlayer.place(relx=0.5, rely=0.35, anchor="center")
    sliderPlayer.set(2)

    labelSlidePlayer = CTkLabel(master=frame, text="2 joueurs", font=("Arial", 20), text_color="#B9C0CC")
    labelSlidePlayer.place(relx=0.5, rely=0.41, anchor="center")

    checkboxFS = CTkCheckBox(master=frame, text="Afficher la modélisation 2D", font=("Arial", 25), text_color="#B9C0CC")
    checkboxFS.place(relx=0.5, rely=0.5, anchor="center")

    checkboxPVP = CTkCheckBox(master=frame, text="PVP (mode combat)", font=("Arial", 25), text_color="#B9C0CC")
    checkboxPVP.place(relx=0.5, rely=0.58, anchor="center")

    labelServer = CTkLabel(master=frame, text="Entrez le nom du serveur:", font=("Arial", 30), text_color="#B9C0CC")
    labelServer.place(relx=0.35, rely=0.67, anchor="center")

    entryServerName = CTkEntry(master=frame, placeholder_text="Ex: Vinland")
    entryServerName.place(relx=0.7, rely=0.67, anchor="center")

    labelPseudo = CTkLabel(master=frame, text="Entrez votre pseudo:", font=("Arial", 30), text_color="#B9C0CC")
    labelPseudo.place(relx=0.35, rely=0.77, anchor="center")

    entryPseudo = CTkEntry(master=frame, placeholder_text="Ex: Flyzerta")
    entryPseudo.place(relx=0.65, rely=0.77, anchor="center")

    btnCreate = CTkButton(master=frame, text="Créer la partie", font=("Arial", 30), command=createGame, border_spacing=10)
    btnCreate.place(relx=0.5, rely= 0.9, anchor="center")

    current_widgets = {"btnBack": btnBack, "label": label, "frame": frame, "label2": label2, "sliderSize": sliderSize, "labelSlideSize": labelSlideSize, "sliderPlayer": sliderPlayer, "labelSlidePlayer": labelSlidePlayer, "btnCreate": btnCreate, "checkboxFS": checkboxFS, "labelPseudo": labelPseudo, "entryPseudo": entryPseudo, "labelServer": labelServer, "entryServerName": entryServerName, "checkboxPVP": checkboxPVP}

def menu_guest():
    """Crée le menu pour rejoindre une partie multijoueur"""
    global current_widgets

    btnBack = CTkButton(master=app, text="Retour", font=("Arial", 30), command=lambda: changeWindow("multi"))
    btnBack.place(relx=0.1, rely=0.1)

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.2, anchor="center")

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=300)
    frame.pack(expand=True)

    labelServer = CTkLabel(master=frame, text="Entrez le nom du serveur:", font=("Arial", 30), text_color="#B9C0CC")
    labelServer.place(relx=0.25, rely=0.2, anchor="center")

    entryServerName = CTkEntry(master=frame, placeholder_text="Ex: Vinland")
    entryServerName.place(relx=0.65, rely=0.2, anchor="center")

    labelPseudo = CTkLabel(master=frame, text="Entrez votre pseudo:", font=("Arial", 30), text_color="#B9C0CC")
    labelPseudo.place(relx=0.25, rely=0.42, anchor="center")

    entryPseudo = CTkEntry(master=frame, placeholder_text="Ex: Flyzerta")
    entryPseudo.place(relx=0.6, rely=0.42, anchor="center")

    btnJoin = CTkButton(master=frame, text="Rejoindre la partie", font=("Arial", 30), command=lambda: creer_un_thread(joinGame), border_spacing=10, width=255, height=65)
    btnJoin.place(relx=0.5, rely= 0.7, anchor="center")

    checkboxFS = CTkCheckBox(master=frame, text="Afficher la modélisation 2D", font=("Arial", 25), text_color="#B9C0CC")
    checkboxFS.place(relx=0.5, rely=0.89, anchor="center")

    current_widgets = {"btnBack": btnBack, "label": label, "frame": frame, "entryServerName": entryServerName, "btnJoin": btnJoin, "checkboxFS": checkboxFS, "labelPseudo": labelPseudo, "entryPseudo": entryPseudo, "labelServer": labelServer, "entryServerName": entryServerName}

def menu_waiting():
    """Crée le menu où les joueurs attendent que touyt le monde ai rejoint la partie"""
    global current_widgets, clientIDn, serverName, lstPseudo

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.1, anchor="center")

    label2 = CTkLabel(master=app, text=f"En attente des autres joueurs ({clientID+1}/{nbPlayer})", font=("Arial", 40), text_color="#FFCC70")
    label2.place(relx=0.5, rely=0.22, anchor="center")

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=200)
    frame.pack(expand=True)

    label3 = CTkLabel(master=app, text=f"Nom du serveur: {serverName}", font=("Arial", 40), text_color="#FFCC70")
    label3.place(relx=0.5, rely=0.33, anchor="center")
    
    label4 = CTkLabel(master=frame, text="Joueurs connectés:", font=("Arial", 30), text_color="#FFCC70")
    label4.place(relx=0.3, rely=0.15, anchor="center")

    current_widgets = {"label": label, "label2": label2, "frame": frame, "label3": label3, "label4": label4}

    lstPseudo.append(pseudo)
    for i in range(nbPlayer):
        if i < len(lstPseudo): texte = f"-> {lstPseudo[i]}"
        else: texte = ""
        if i == clientID: texte += " (vous)"
        current_widgets[f"label{6+i}"] = CTkLabel(master=frame, text=texte, font=("Arial", 20), text_color="#FFCC70")
        current_widgets[f"label{6+i}"].place(relx=0.3, rely=0.3 + i * 0.12)


    #! Il faut que le serveur collecte en même temps les joueurs qui se connectent
    #! Lorsque tous les joueurs sont connectés: écran "Lancement de la partie dans 3-2-1" puis on lance le jeu

def menu_principal():
    """Crée le menu d'accueil"""

    global current_widgets

    frame = CTkFrame(master=app, fg_color="transparent", border_color="#FFCC70", border_width=2, width=900, height=250)
    frame.pack(expand=True)

    btnQuitter = CTkButton(master=app, text="Quitter", font=("Arial", 30), command= app.destroy)
    btnQuitter.place(relx=0.1, rely=0.1)

    label = CTkLabel(master=app, text="Le l4byrinth3D", font=("Arial", 50), text_color="#FFCC70")
    label.place(relx=0.5, rely=0.2, anchor="center")

    label2 = CTkLabel(master=frame, text="Choisissez le mode:", font=("Arial", 40), text_color="#B9C0CC")
    label2.place(relx=0.5, rely=0.2, anchor="center")

    btnSolo = CTkButton(master=frame, text="Jouer en solo", font=("Arial", 30), corner_radius=5, fg_color="#3F5477", hover_color="#4158D0", border_color="#FFCC70", border_width=2,command=lambda: changeWindow("solo"), border_spacing=10)
    btnSolo.place(relx=0.3, rely=0.7, anchor="center")

    btnMulti = CTkButton(master=frame, text="Jouer en multijoueur", font=("Arial", 30), corner_radius=5, fg_color="#3F5477", hover_color="#4158D0", border_color="#FFCC70", border_width=2, command=lambda: changeWindow("multi"), border_spacing=10)
    btnMulti.place(relx=0.7, rely=0.7, anchor="center")
   
    current_widgets = {"frame": frame, "label": label, "label2": label2, "btnSolo": btnSolo, "btnMulti": btnMulti, "btnQuitter": btnQuitter}

def changeWindow(newWindow:str):
    """ changeWindow supprime tous les widgets de la page puis appelle une autre fonction pour changer de menu
        :param newWindow: une chaîne de caractère parmis les suivantes "solo", "multi", "main", "host", "guest", "waiting"
        qui décidera de la fonction à appeler (et donc du menu à afficher)
    """

    global current_widgets

    for k in current_widgets.keys(): current_widgets[k].destroy()

    if newWindow == "solo": menu_solo()
    elif newWindow == "multi": menu_multi()
    elif newWindow == "main": menu_principal()
    elif newWindow == "host": menu_host()
    elif newWindow == "guest": menu_guest()
    elif newWindow == "waiting": menu_waiting()

changeWindow("main")
app.mainloop()