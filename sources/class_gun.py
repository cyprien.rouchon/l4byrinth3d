class Gun():
    def __init__(self,nb_bullets):
        """
        initialise les variables de Gun
        :param nb_bullets: str, le nombre maximum de balle dans un chargeur et le nombre initial de balle dans se même chargeur
        """
        self.nb_bullets = nb_bullets
        self.nb_bullets_max = nb_bullets

    def reload(self):
        """
        permet de remplir le chargeur
        """
        self.nb_bullets = self.nb_bullets_max

    def shot(self):
        """
        vide le chergeur d'une balle
        :return: True si il reste une balle ou plus avant le tire, False si le chargeur est vide avant le tire
        """
        if self.nb_bullets != 0:
            self.nb_bullets -= 1
            return True
        else: return False

    def isLoaded(self):
        """
        :return: True si le chargeur n'est pas vide, False sinon
        """
        return self.nb_bullets > 0
