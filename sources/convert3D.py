from divers import *
from math import sin, cos, radians

def findIntersection(CD, A, B, CD_is_segment=True):
    """
    findIntersection renvoie le point qui est l'intsersection entre le segment (ou la droite) CD et la demi-droite [AB) s'il en existe une (et renvoie sinon "None")
    :param CD: segment ou droite (dans le cadre du labyrinthe le segment représente en majeur partie du temps un mur)
    :param A: tuples des coordonnées (x,y) d'un point (dans le contexte du labyrinthe, A sera toujours la position du joueur)
    :param B: tuples des coordonnées (x,y) d'un point (dans le contexte du labyrinthe, B un point qui est une projection de la vue du joueur)
    :param CD_is_segment: booléen qui indique si CD est ici un segment ou une droite
    :return: un tuple de float correspondant à l'intersection s'il en exite une, "None" sinon
    """

    C = CD[0]
    D = CD[1]

    intersection = "None"

    if C[0] == D[0] or C[1] == D[1]: # CD est verticale ou horizontale

        if B[0] == A[0]: intersection = (A[0], C[1]) # AB est verticale
        elif B[1] == A[1]: intersection = (C[0], A[1])# AB est horizonrale
        elif C[0] == D[0]: # CD est verticale
            penteAB = (B[1]-A[1]) / (B[0]-A[0]) # coefficient directeur de la droite AB
            dist_origine_penteAB = A[1] - (penteAB * A[0])
            intersection = (C[0],  penteAB * C[0] + dist_origine_penteAB)
        else: # CD est horizontale
            penteAB = (B[1]-A[1]) / (B[0]-A[0]) # coefficient directeur de la droite AB
            dist_origine_penteAB = A[1] - (penteAB * A[0])
            intersection = ((C[1] - dist_origine_penteAB)/penteAB, C[1])
    else:
        penteCD = (D[1] - C[1]) / (D[0] - C[0])  # coefficient directeur de CD
        dist_origine_penteCD = C[1] - (penteCD * C[0]) # disatance à l'origibe de CD

        if B[0] == A[0]: #la vision est verticale
            if not CD_is_segment or C[1] <= penteCD*A[0] + dist_origine_penteCD <= D[1]: intersection = (A[0], penteCD*A[0] + dist_origine_penteCD) # si CD est une droite ou si l'intersection appartient au segment CD: on a bel bien trouvé l'intersection
        elif B[1] == A[1]: #la vision est horizontale
            if not CD_is_segment or C[1] <= A[1] <= D[1]: intersection = ((A[1] - dist_origine_penteCD)/penteCD, A[1]) # si CD est une droite ou si l'intersection appartient au segment CD: on a bel bien trouvé l'intersection
        else: 
            penteAB = (B[1]-A[1]) / (B[0]-A[0]) # coefficient directeur de la droite AB
            dist_origine_penteAB = A[1] - (penteAB * A[0])
            if penteAB != penteCD:
                x_intersection = (dist_origine_penteCD - dist_origine_penteAB) / (penteAB - penteCD) # abscisse du point d'intersecttion des 2 droite (s'il existe)
                if not CD_is_segment or C[0] <= x_intersection <= D[0]: 
                    intersection = (x_intersection, penteAB * x_intersection + dist_origine_penteAB)

        if intersection != "None" and distancePoints(A, intersection) < distancePoints(B, intersection): intersection = "None" # on vérifie que l'intersection est bien sur [AB) et non uniquement sur (AB)

    return intersection

def abscisse2DsegmentHorizontal(WIDTH, xyJ, vueJ, pt1, pt2):
    """
    abscisse2DsegmentHorizontal renvoie les abscissies qui correspondent à l'affichage d'un segment [pt1, pt2] horizontal du plan 3D en fonction donc du joueur et de son orientation de la vue (horizontal ici)
    :param: WIDTH: largeur de la fenêtre où s'affiche la représentation 3D
    :param xyJ: tuples des coordonnées (x,y) du joueur
    :param vueJ: tuples des coordonnées (x,y) d'un point corespondant à une projection horizontal de la vue du joueur
    :param pt1: tuples des coordonnées (x,y) d'un point (une des deux extrémitées du segment en l'occurence)
    :param pt2: tuples des coordonnées (x,y) d'un point (l'autre extrémitée du segment)
    :return: un tuple de 4 valeurs: (x1, x2, angleABDH, angleABD2H)
    x1 et x2 correspondent donc aux abscisses de l'affichage de la représentation 3D du segment
    angleABDH et angleABD2H correspondent respectivement aux angles (pt1,xyJ,vueJ) et (pt2, xyJ, vueJ)
    """
    angleABDH, angleABD2H = calculAngle(pt1,xyJ,vueJ), calculAngle(pt2, xyJ, vueJ) # H pour horizontale
    x,y = (xyJ)
    if (pt1[0] == pt2[0] and x > pt1[0]) or (pt1[1] == pt2[1] and y < pt1[1]): angleABDH, angleABD2H = angleABD2H, angleABDH
    if pt1[0] != pt2[0] and pt1[1] != pt2[1] and findIntersection(((pt1[0], pt1[1]), (pt2[0], pt2[1])), xyJ, (xyJ[0]-0.1, xyJ[1]), False) != "None": angleABDH, angleABD2H = angleABD2H, angleABDH

    angleABDH = 90 - ((angleABDH + 45) % 360)
    distanceH = angleABDH/90
    if distanceH < 0: distanceH = 0

    angleABD2H = 90 - (360 -  ((angleABD2H - 45) % 360))
    distanceB = angleABD2H/90
    if distanceB < 0: distanceB = 0 

    x1 = WIDTH * distanceH
    x2 = WIDTH - (WIDTH*distanceB)
    return x1, x2, angleABDH, angleABD2H

def ordonnées2DsegmentHorizontal(HEIGHT, j, vueJ, pt1, pt2, inters1, inters2, FS):
    """
    ordonnées2DsegmentHorizontal renvoie les ordonnées qui correspondent à l'affichage d'un segment [pt1, pt2] horizontal du plan 3D en fonction donc du joueur et de son orientation de la vue (horizontalet vertical ici)
    :param HEIGHT: hauteur de la fenêtre où s'affiche la représentation 3D
    :param j: le joueur (objet de la classe Joueur)
    :param vueJ: tuples des coordonnées (x,y) d'un point corespondant à une projection horizontal de la vue du joueur
    :param pt1: tuples des coordonnées (x,y) d'un point (une des deux extrémitées du segment en l'occurence)
    :param pt2: tuples des coordonnées (x,y) d'un point (l'autre extrémitée du segment)
    :param inters1: tuples des coordonnées (x,y) d'un point (inters1 et inters2 sont les points ayant pour coordonnées l'intsersection entre les limites du champ de vision du joueur et le mur) 
    (le mur étant le l'usage le plus fréquent mais en réalité cela peut également correspondre à un des rectangles de la représentation 3D des autres joueurs)
    :param inters2: tuples des coordonnées (x,y) d'un point
    :param FS: booléen indiquant si l'affichage 3D se fait sur toute la fenêtre ou seulement sur la deuxième moitié (l'écran étant alors coupé horizontalment)
    :return: un tuple de 2 valeurs: (y1, y2)
    y1 et y2 correspondent donc aux ordonnées de l'affichage de la représentation 3D du segment
    """
    x,y = j.x,j.y

    angleBCM1 = calculAngle((pt1[0], pt1[1]), (x,y), vueJ) # angle antre le 1er point du mur, le joueur et l'axe de vision du joueur
    angleBCM2 = calculAngle((pt2[0], pt2[1]), (x,y), vueJ) # angle antre le 2ème point du mur, le joueur et l'axe de vision du joueur

    if inters1 == "None": 
        if angleBCM1 < 45 or angleBCM1 > 315: inters1 = (pt1[0], pt1[1])
        else: inters1 = (pt2[0], pt2[1])
    
    if inters2 == "None": 
        if angleBCM2 < 45 or angleBCM2 > 315: inters2 = (pt2[0], pt2[1])
        else: inters2 = (pt1[0], pt1[1])

    if pt1[0] == pt2[0] or pt1[1] == pt2[1]:
        if inters1[0] > inters2[0]: inters1, inters2 = inters2, inters1
        elif inters1[1] > inters2[1]: inters1, inters2 = inters2, inters1
    elif (inters1 == (pt1[0], pt1[1]) or inters2 == (pt2[0], pt2[1])) and findIntersection(((pt1[0], pt1[1]), (pt2[0], pt2[1])), (x,y), (x-0.1, y), False) != "None": inters1, inters2 = inters2, inters1

    A = (0, j.size)
    B = (cos(radians(j.orientationVerticale))*10, A[1] + sin(radians(j.orientationVerticale))*10) # projection de la vision du joueur (horizontale)
    C1 = (distancePoints((x,y), inters1), pt1[2])
    C2 = (distancePoints((x,y), inters2), pt1[2])

    angleC1AB, angleC2AB = calculAngle(C1,A,B), calculAngle(C2,A,B)

    if (pt1[0] == pt2[0] and x > pt1[0]) or (pt1[1] == pt2[1] and y < pt1[1]): angleC1AB, angleC2AB = angleC2AB, angleC1AB

    if j.size > pt1[2]:
        angleC1AB = 90 - ((angleC1AB + 45) % 360)
        angleC2AB = 90 - ((angleC2AB + 45) % 360)
    else:
        angleC1AB = 90 - (360 - ((angleC1AB - 45) % 360))
        angleC2AB = 90 - (360 - ((angleC2AB - 45) % 360))

    distance1 = angleC1AB/90
    distance2 = angleC2AB/90

    if distance1<0 : distance1 = 0
    if distance2<0 : distance2 = 0

    if not FS:
        if j.size > pt1[2]:
            y1 = HEIGHT*2 - (HEIGHT * distance1)
            y2 = HEIGHT*2 - (HEIGHT * distance2)
        else:
            y1 = HEIGHT + HEIGHT * distance1
            y2 = HEIGHT + HEIGHT * distance2
    else:
        if j.size > pt1[2]:
            y1 = HEIGHT - (HEIGHT * distance1)
            y2 = HEIGHT - (HEIGHT * distance2)
        else:
            y1 = HEIGHT * distance1
            y2 = HEIGHT * distance2

    return y1, y2

def verticalRect(j, WIDTH:int, HEIGHT:int, FS:bool, pt1, pt2):
    """La fonction transforme les coordonnées 3D d'un rectangle vertical en coordonnées de la représentation 3D à l'écran de ce rectangle en fonction de la vue du joueur
        :param j: le joueur (type: objet de la classe joueur)
        :param WIDTH: largeur de l'écran (type: int)
        :param HEIGHT: largeur de l'écran (type: int)
        :param FS: affichage sur le plein écran (True) ou affichage sur la deuxième moitié de l'écran (False) -> utile lorsqu'on veut afficher en haut de l'écran la représentation 2D
        :param pt1: tuple (x:float, y:float, z:float) sommet "bas gauche" du rectangle
        :param pt2: tuple (x:float, y:float, z:float) sommet du rectangle opposé à pt1
        :return: un dictionnaire tel que: {
            "x1": float, "x2": float,
            "y1": float, "y2": float, "y3": float, "y4": float, 
            "draw": bool (True si il faut afficher le mur, False sinon car il n'apparaît pas dans la vue du joueur)
        }"""
    
    if not FS: HEIGHT/=2
    x,y,oriH = j.x, j.y, j.orientationHorizontale
    A = (x,y)   # position du joueur
    B = (x + cos(radians(oriH))*10, y + sin(radians(oriH))*10) # projection de la vision du joueur (horizontale)
    
    mur = {}
    mur["x1"], mur["x2"], angleABDH, angleABD2H = abscisse2DsegmentHorizontal(WIDTH, A, B, pt1, pt2)

    #intersection_mur_j_C1 et intersection_mur_j_C2 sont les points ayant pour coordonnées l'intsersection entre les limites du champ de vision du joueur et le mur
    B1, B2 = (x + cos(radians(oriH-45))*10, y + sin(radians(oriH-45))*10), (x + cos(radians(oriH+45))*10, y + sin(radians(oriH+45))*10) # projection des limites du champs de vision +45° et -45° 

    intersection_mur_j_C1 = findIntersection(((pt1[0], pt1[1]), (pt2[0], pt2[1])), A, B1)
    intersection_mur_j_C2 = findIntersection(((pt1[0], pt1[1]), (pt2[0], pt2[1])), A, B2)

    if pt1[0] == pt2[0] or pt1[1] == pt2[1]:
            if angleABDH >= 0: intersection_mur_j_C1 = "None"
            if angleABD2H >= 0: intersection_mur_j_C2 = "None"

    #! ------------------------ Calcule angle de vue verticale ---------------------------------------

    mur["y1"], mur["y2"] = ordonnées2DsegmentHorizontal(HEIGHT, j, B, pt1, (pt2[0], pt2[1], pt1[2]), intersection_mur_j_C1, intersection_mur_j_C2, FS)
    mur["y3"], mur["y4"] = ordonnées2DsegmentHorizontal(HEIGHT, j, B, (pt1[0], pt1[1], pt2[2]), pt2, intersection_mur_j_C1, intersection_mur_j_C2, FS)

    mur["draw"] = not (angleABDH < 0 and angleABD2H < 0) or (angleABDH> -90 and angleABD2H>-90) # O, vérifie si le mur appararaît bien dans lme champ de vision horizontal
    return mur