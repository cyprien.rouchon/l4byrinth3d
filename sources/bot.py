import ftplib

HOSTNAME = "ftpupload.net"
USERNAME = "ezyro_36166421"
PASSWORD = "67da4239244d6"

def connexion():
    """
    creer une connexion ftp avec le service de resolution de nom de serveur hebergé sur un serveur web
    :return: objet ftplib
    """
    ftp_server = ftplib.FTP(HOSTNAME, USERNAME, PASSWORD, timeout=100)
    ftp_server.encoding = "utf-8"

    ftp_server.cwd("/htdocs")
    return ftp_server

def download():
    """
    telecharge le fichier index.txt present sur le serveur web via le protocole FTP
    """
    ftp_server = connexion()
    filename = "index.txt"
    with open(filename, "wb") as file:
        ftp_server.retrbinary(f"RETR {filename}", file.write)
    ftp_server.quit()


def upload():
    """
    envoie le fichier index.txt present en local sur le serveur web via le protocole FTP
    """
    ftp_server = connexion()
    filename = "index.txt"
    with open(filename, "rb") as file:
        ftp_server.storbinary(f"STOR {filename}", file)
    ftp_server.quit()


def add_server(server="test|0.tcp.eu.ngrok.io:1034"):
    """
    add_server ajoute un serveur dans la liste si un serveur n'est pas deja lancé sur l'ip ou si le nom du serveur n'existe pas deja
    :param server: str, de la forme "nom_du_serveur:adresse_du_serveur_ngrok:port"
    """
    ftp_server = connexion()
    if check_name(server.split("|")[0]):
        file = open("index.txt","a")
        file.writelines([f"{server}\n"])
        file.close()
        upload()
        ftp_server.quit()
        return True
    else:
        print("un serveur identique existe deja")
        ftp_server.quit()
        return False

def change_server_addr(server_name, addr):
    """
    echange l'adresse lié à nom de serveur déjà existant
    :param server_name: str, le nom du serveur
    :param addr: str, la nouvelle addresse
    """
    ftp_server = connexion()
    lines = open("index.txt","r").readlines()
    for i,line in enumerate(lines):
        if server_name == line.split("|")[0]:
            lines[i] = f"{server_name}|{addr}\n"
            file = open("index.txt","w")
            file.writelines(lines)
            file.close()
            upload()
            ftp_server.quit()
            return
    print(f"le serveur {server_name} n'existe pas")
    ftp_server.quit()
    return


def check_name(server_name):
    """
    check verifie si un seveur existe deja
    :param server_name: str, nom du serveur
    :return: bool, True si le serveur n'existe pas, False si le serveur existe
    """
    ftp_server = connexion()
    download()
    file = open("index.txt","r")
    for line in file.readlines():
        check_name, check_addr = line.split("|")

        if server_name == check_name:
            file.close()
            ftp_server.quit()
            return False
        
    file.close()
    ftp_server.quit()
    return True


def addr_from_name(name):
    """
    renvoie l'addresse lié à un nom de serveur
    :param name: str, le nom du serveur dont on cherche l'addresse
    :return: str, l'addresse qui est associé au nom du serveur
    """
    ftp_server = connexion()
    download()
    file = open("index.txt","r")
    for line in file.readlines():
        server_name, addr = line.split("|")
        if name == server_name:
            ftp_server.quit()
            return addr
    ftp_server.quit()
    return "ce serveur n'existe pas"

def supr_server(server_name):
    """
    permet de suprimer un serveur de la liste, nom et addresse
    :param server_name: str, nom du serveur que l'on veut suprimer
    """
    ftp_server = connexion()
    lines = open("index.txt","r").readlines()
    for i,line in enumerate(lines):
        if server_name == line.split("|")[0]:
            lines[i] = f""
            file = open("index.txt","w")
            file.writelines(lines)
            file.close()
            upload()
            ftp_server.quit()
            return
    print(f"le serveur {server_name} n'existe pas")
    ftp_server.quit()
    return


if __name__ == "__main__":
    #tests quand le fichier n'est pas un import
    upload()
    