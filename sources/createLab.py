import math
from random import random, shuffle

def deadEnd(chem, c, longC:int):
    """
        deadEnd regarde si la  case actuelle est entourée de case du chemin
        :param chem: le chemin
        :param c: la case actuelle
        :param longC: la taille du labyrinthe
        :return: True si c'est un cul-de-sac, False sinon
    """
    if (c[0]+1, c[1]) not in chem and not c[0] == longC-1:  return False
    if (c[0], c[1]+1) not in chem and not c[1] == longC-1:  return False
    if (c[0], c[1]-1) not in chem and not c[1] == 0:  return False
    if (c[0]-1, c[1]) not in chem and not c[0] == 0:  return False
    return True
    
def directionsOuvertes(c1, c2):
    """
        directionsOuvertes regarde si les deux cases sont à côtés et si oui dans quelle direction se trouve c2 par rapport à c1
        :param c1: une case du labyrinthe
        :param c2: une autre case du labyrinthe
        :return: None si les cases ne sont pas voisines, sinon "Nord", "SUD", "OUEST" ou "EST" en fonction de la position de c2 par rapport à c1
    """
    if (c1[0], c1[1]) == (c2[0]+1, c2[1]): return "NORD"
    elif (c1[0], c1[1]) == (c2[0]-1, c2[1]): return "SUD"
    elif (c1[0], c1[1]) == (c2[0], c2[1]+1): return "OUEST"
    elif (c1[0], c1[1]) == (c2[0], c2[1]-1): return "EST"
    else: return None

def casserDesMurs(grilleLab, probaM=(0.78, 0.87)):
    """
    casserDesMurs (fonction à effet de bord) permet d'enlever aléatoirement des murs dans le labyrinthe afin que le chemin soit moins linéaire et que le labyrinthe soit plus ouvert
    :param grilleLab: la grille du labyrinthe
    :param probaM: tuple de 2 floats (<=1) qui correspondent resp. à la proba de ce conserver un mur dans une case totalement fermée (avec 4 murs) et à celle dans une case qui n'est pas complétement fermée
    :return: grilleLab (modifiée)
    """
    for y in range(len(grilleLab)):
        for x in range(len(grilleLab)):
            if True in grilleLab[y][x]:
                if grilleLab[y][x] == [True]*4: proba = probaM[0]
                else: proba = probaM[1]
                murCassable = []
                if x != 0: murCassable.append(1)
                if y != 0: murCassable.append(0)
                if x != len(grilleLab) - 1: murCassable.append(3)
                if y != len(grilleLab) - 1: murCassable.append(2)
                if not len(murCassable) == 0:
                    shuffle(murCassable)
                    for mur in murCassable:
                        if grilleLab[y][x].count(True) == 1: proba = 0.99
                        if random() > proba: 
                            grilleLab[y][x][mur] = False
                            if mur == 0 and y!=0: grilleLab[y-1][x][2] = False
                            if mur == 1 and x!=0: grilleLab[y][x-1][3] = False
                            if mur == 2 and y!=len(grilleLab)-1: grilleLab[y+1][x][0] = False
                            if mur == 3 and x!=len(grilleLab)-1: grilleLab[y][x+1][1] = False
                            proba = (1 - proba)/2 + proba

    return grilleLab
    
def genererChemLab(size:int, PVP=False, case_depart = (0,0), case_objectif=None):
    """
        genererChemLab génére une chemin aléatoire de la case de départ (0,0) à la case de sortie généré également aléatoirement sur la dernière colonne ou la dernière ligne du labyrinthe
        :param size: la taille du labyrinthe (longueur d'un côté)
        :param PVP: la labyrinthe n'a plus de sortie () et renvoie un case de spawn
        :return: {"chemin": chemin, "rac": raccourcis, "taille": tailleLab, "sortie": caseSortie} 

        "chemin": est une liste de tuple représentant les cases par lesquels passe le chemin
        "rac": est une liste de dictionnaire représentant tous les raccourcis du chemin (un raccourci est un retour à une case précédente qui n'est pas forcément voisine car le chemin arrivait à un cul-de-sac)
        "taille": pareil que le param size
        "sortie": la case de sortie
    """

    tailleLab = size
    # La case de sortie du labyrinthe est généré aléatoirement sur la dernière ligne et/ou sur la dernière colonne
    if not PVP: caseSortie = (tailleLab-1, math.ceil(random() * tailleLab-1)) if random()>0.5 else (math.ceil(random() * tailleLab-1), tailleLab-1) 
    else: caseSortie = case_objectif
    raccourcis = []

    chemin = [case_depart]
    cheminFound = False

    while not cheminFound:

        newCase = None     
        currentCase = None
        changeDeCase = {"currently": False, "casePrec": None, "nvCase": None, "caseAssociee": None}

        if not deadEnd(chemin, chemin[-1], tailleLab): currentCase = chemin[-1]
        else:
            i = 1
            while currentCase == None:
                i+=1
                if not deadEnd(chemin, chemin[-i], tailleLab): 
                    currentCase = chemin[-i]
                    changeDeCase["currently"] = True
                    changeDeCase["casePrec"] = chemin[-1]
                    changeDeCase["nvCase"] = chemin[-i]
                if i == len(chemin):
                    currentCase = "echec"
                    return "echec"

        plafond, sol, murO, murE = currentCase[0] == 0, currentCase[0] == tailleLab-1, currentCase[1] == 0, currentCase[1] == tailleLab-1
        
        directionPoss = []
        if not plafond and (currentCase[0] - 1, currentCase[1]) not in chemin: directionPoss.append((-1, 0)) # Aller vers le nord
        if not sol and (currentCase[0] + 1, currentCase[1]) not in chemin: directionPoss.append((1, 0)) # Aller vers le sud
        if not murO and (currentCase[0], currentCase[1]-1) not in chemin: directionPoss.append((0, -1))
        if not murE and (currentCase[0], currentCase[1]+1) not in chemin: directionPoss.append((0, 1))

        # Aller vers la sortie si elle se trouve à portée
        for d in directionPoss:
            if (currentCase[0] + d[0], currentCase[1] + d[1]) == caseSortie: 
                newCase = tuple(caseSortie)

        # Choisir une direction au hasard parmi les possibilités
        if newCase == None:
            direction = directionPoss[math.floor(random() * len(directionPoss))]
            newCase = (currentCase[0] + direction[0], currentCase[1] + direction[1])

            if changeDeCase["currently"]:
                changeDeCase["caseAssociee"] = newCase
                raccourcis.append(changeDeCase)

        if newCase != None and newCase not in chemin: chemin.append(newCase)
        if chemin[-1] == caseSortie: cheminFound = True
    return {"chemin": chemin, "rac": raccourcis, "taille": tailleLab, "sortie": caseSortie}

def genererGrilleLab (chemin, rac, taille:int):
    """
        genererGrilleLab transforme le chemin crée dans le labyrinthe en véritable grille de labyrinthe avec la structure suivante:
        ex (3*3):   
                    [[[T, F, F, T], [F, T, T, T], [F, F, T, F]],                 (F: False | T: True)
                    [[T, F, F, F], [T, T, T, T], [F, F, T, T]],         (False: pas de mur | True: présence d'un mur)
                    [[F, T, F, T], [T, F, F, T], [F, F, F, T]]]         (dans l'ordre suivant: ["NORD", "OUEST", "SUD", "EST"])
        
        :param chemin: liste de tuple représentant un chemin (voir genererGrilleLab())
        :param rac: liste des raccourcis du chemin (voir genererGrilleLab())
        :param taille: taille du labyrinthe
        :return: grille du labyrinthe dans le format décrit ci-dessus
    """
    # La fonction transforme le chemin crée dans le labyrinthe en véritable grille de labyrinthe avec l'implémentation vu précdemment
    
    T, F = True, False

    # On commence par créer la grille du labirynthe en la remplissant de murs
    grilleLab = []
    for i in range(taille):
        grilleLab.append([])
        for u in range(taille):
            grilleLab[-1].append([T, T, T, T])

    direction = {"NORD": 0, "OUEST": 1, "SUD": 2, "EST": 3}
    racFound = 0 # Nombre de raccourcis utilisé

    for i in range(len(chemin)):
        ouvrir = "undefined"

        c = chemin[i]
        if c != chemin[-1]: ouvrir = directionsOuvertes(c, chemin[i+1])
        else: 
            grilleLab = casserDesMurs(grilleLab)
            return grilleLab
        c2 = chemin[i+1]

        if ouvrir == None:

            if rac[racFound]["casePrec"] == c and rac[racFound]["caseAssociee"] == c2:
                #print("Raccourci trouvé")
                c = rac[racFound]["nvCase"]
                c2 = rac[racFound]["caseAssociee"]
                ouvrir = directionsOuvertes(c, c2)
                grilleLab[c[0]][c[1]][direction[f"{ouvrir}"]] = F
                grilleLab[c2[0]][c2[1]][(direction[f"{ouvrir}"]+2)%4] = F
                #print(f"--Raccourci-- Ligne: {c[0]}\nColone: {c[1]}\nDirection: {direction[ouvrir]}")
                racFound+=1
            else: print(f"Problème de raccourci\n ouvrir: {ouvrir} c: {c}")
                
        else:
            grilleLab[c[0]][c[1]][direction[f"{ouvrir}"]] = F
            grilleLab[c2[0]][c2[1]][(direction[f"{ouvrir}"]+2)%4] = F
            #print(f"Ligne: {c[0]}\nColone: {c[1]}\nDirection: {direction[ouvrir]}")

    grilleLab = casserDesMurs(grilleLab)
    return grilleLab

def createLaby(size, PVP=False):
    """
        createLaby créer un labyrinthe aléatoirement de taille carré (size*size)
        :param size: taille d'un côté du labyrinthe
        :param PVP: booléen -> le labyrinthe est-il généré pour une partie de PVP?
        :return: grilleLab, chemGenere["sortie"] -> grille du labyrinthe dans le format décrit dans genererGrilleLab() et le tuple représentant la case de sortie
    """
    chemGenere = genererChemLab(size)
    grilleLab = genererGrilleLab(chemGenere["chemin"], chemGenere["rac"], chemGenere["taille"])
    if not PVP: grilleLab = casserDesMurs(grilleLab)
    else: grilleLab = casserDesMurs(grilleLab, (0, 0.78))

    return grilleLab, chemGenere["sortie"]