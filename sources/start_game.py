import pygame
from time import perf_counter
from divers import *
from tkinter import *
from classe_joueur import *
import pygame_widgets
from pygame_widgets.button import Button
from pygame_widgets.slider import Slider
from pygame_widgets.textbox import TextBox
from draw_lab import drawLab, function_back, init
from createLab import createLaby
import os 

dir_path = os.path.dirname(os.path.realpath(__file__))
file = dir_path+"\\gun.mp3"
pygame.mixer.init()
sound_shoot = pygame.mixer.Sound(dir_path+"\\src\\shot_received.mp3")

server, autres_joueurs = [None] * 2

runGame = True
def stop_game():
    global runGame
    runGame = False

def startGame(FS, solo, PVP=False, data = {}, mazeSide = None):
    """
    startSoloGame lance la partie en solo: il ouvre la page pygame, initialise le joueur et lance la boucle principale
    :param mazeSide: la taille du labyrinthe (du côté)
    """
    global start, runGame, wF, hF, frameSettings, widgets, frameEnd, text_fontV, server, autres_joueurs, joueur, runGame
    start = perf_counter()

    runGame = True

    root = Tk()
    HEIGHT, WIDTH = root.winfo_screenheight(), root.winfo_screenwidth()
    root.destroy()

    pygame.init()

    screen = pygame.display.set_mode((WIDTH, HEIGHT), FULLSCREEN)
    
    if solo:
        grilleLab, case_sortie = createLaby(mazeSide)
        joueur = Joueur(30,100,0,0,0,20,2,100,grilleLab)
    else:
        grilleLab = data["grilleLab"]
        case_sortie = data["case_sortie"]
        joueur = data["joueur"]
        autres_joueurs = data["autres_joueurs"]
        server = data["server"]

    # Fenêtre des paramètres
    wF, hF = int(WIDTH/2), int(HEIGHT/1.7)
    frameSettings = pygame.Surface((wF, hF))
    pygame.draw.rect(frameSettings, color=pygame.Color(182, 193, 212, 100), rect=(5, 5,wF-10, hF-10), border_radius= 5)
    frameSettings.set_alpha(230)
    text_fontV = pygame.font.SysFont("verdana", int(wF/15))
    text_fontC = pygame.font.SysFont("calibri", int(hF/15))

    draw_text(frameSettings, "Paramètres", text_fontV, "black", wF/3, 50)
    draw_text(frameSettings, "Sensibilité", text_fontC, "black", wF/3 + 50, 150)

    buttonBack = Button(screen, wF/2 + 0.42 * wF, hF/1.7 + hF/2, wF/6, hF/8, radius=10, text="Retour", onClick=function_back)
    buttonExit = Button(screen, wF/2 + 0.42 * wF, hF/1.7 + hF/3, wF/6, hF/8, radius=10, text="Quitter", onClick=stop_game)
    slider = Slider(screen, int(wF/2 + wF/10), int(hF/1.7 + 20), int(0.8*wF), int(hF/20), min=0, max=100, step=1, initial=int(joueur.sensi*10))
    output = TextBox(screen, wF/1.05, hF/1.7 + int(hF/20)*4, int(wF/10), int(hF/10), fontSize=25)    
    output.disable()
    buttonBack.hide()
    buttonExit.hide()
    slider.hide()
    output.hide()

    frameEnd = pygame.Surface((wF*0.8, hF*0.6))
    pygame.draw.rect(frameEnd, color=pygame.Color(182, 193, 212, 100), rect=(5, 5,wF*0.8-10, hF*0.6-10), border_radius= 5)
    frameEnd.set_alpha(230)
    draw_text(frameEnd, "Vous avez trouvé la sortie!", text_fontC, "black", wF*0.15, hF*0.07)
    draw_text(frameEnd, "Voulez-vous continuer ou quitter le jeu?", text_fontC, "black", wF*0.05, hF*0.15)
    buttonContinue = Button(screen, wF/2 + 0.35 * wF, hF/1.2, wF/6, hF/8, radius=10, text="Continuer", onClick=function_back)
    buttonEnd = Button(screen, wF/2 + 0.35 * wF + (wF/6) * 1.3, hF/1.2, wF/6, hF/8, radius=10, text="Quitter", onClick=stop_game)

    buttonContinue.hide()
    buttonEnd.hide()

    widgets = {"back": buttonBack, "exit": buttonExit, "sensi": slider, "sensiOutput": output, "buttonContinue": buttonContinue, "buttonEnd": buttonEnd}

    init({"widgets": widgets, "wF": wF, "hF": hF, "frameEnd": frameEnd, "text_fontV": text_fontV, "frameSettings": frameSettings}, HEIGHT)
        
    if not solo: creer_un_thread(network)
    mouse.set_visible(False)
    while runGame:
        events = pygame.event.get()
        for pyEvent in events:
            if pyEvent.type == QUIT:
                runGame = False
        screen.fill((200,200,200))
        if solo: drawLab(joueur, WIDTH, HEIGHT, screen, grilleLab, case_sortie, multi=False, FS=FS)
        else: drawLab(joueur, WIDTH, HEIGHT, screen, grilleLab, case_sortie, autres_joueurs=autres_joueurs, server=server, FS=FS, PVP=PVP)
        pygame_widgets.update(events)
        pygame.display.update()

    pygame.quit()

def network():
    """network gère la reception de message du serveur"""
    global i, server, autres_joueurs, joueur
    while True:
        try:
            msg1 = server.recv()
            
            msg = msg1.split("m") #si un joueur se déplace
            if len(msg) > 1:
                msg = msg[-1:]
                msg = msg[0].split(",")[1:5]
                var1,var2,var3,var4 = msg
                x = float(var2)
                y = float(var3)
                autres_joueurs[int(var1)].setCoo((x,y))
            
            msg = msg1.split("o")# si l'orientation d'un joueur change
            if len(msg) > 1:
                msg = msg[-1:]
                msg = msg[0].split(",")[1:5]
                var1,var2,var3,var4 = msg
                orientationH = float(var2)
                autres_joueurs[int(var1)].setOrientationH(orientationH)

            msg = msg1.split("mo")# si un joueur se déplace et change d'orientation en même temps
            if len(msg) > 1:
                msg = msg[-1:]
                msg = msg[0].split(",")[1:5]
                var1,var2,var3,var4 = msg
                x = float(var2)
                y = float(var3)
                orientationH = float(var4)
                autres_joueurs[int(var1)].setOrientationH(orientationH)
                autres_joueurs[int(var1)].setCoo((x,y))

            msg = msg1.split("t")#si un joueur tire
            if len(msg) > 1:
                msg = msg[-1:]
                msg = msg[0].split(",")[1:5]
                var1,var2,var3,var4 = msg
                x = float(var1)
                y = float(var2)
                sound_shoot.set_volume((dist((x,y),(joueur.x,joueur.y)))**(-1/5))
                sound_shoot.play()
                
            msg = msg1.split("d")#si un joueur fait des dégats à un autre joueur
            if len(msg) > 1:
                msg = msg[-1:]
                msg = msg[0].split(",")[1:5]
                var1,var2,var3,var4 = msg
                x = float(var1)
                y = float(var2)
                id = int(var3)
                degat = int(var4)
                sound_shoot.set_volume((dist((x,y),(joueur.x,joueur.y)))**(-1/5))
                sound_shoot.play()
                if id == joueur.id:
                    print(degat)
                    joueur.degat(degat)

            msg = msg1.split("k")#si un joueur en tue un autre 
            if len(msg) > 1:
                msg = msg[-1]
                msg = msg.split(",")[1]
                killer = int(msg)
                if killer != joueur.id: autres_joueurs[killer].nbEliminations += 1
            
        except Exception as e:# avec le module socket certains msg peuvent mal s'envoyés
            print(e)
            continue
            